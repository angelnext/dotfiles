function fish_prompt
    set -l symbol '$ '
    if fish_is_root_user
        set symbol '# '
        set -q fish_color_cwd_root
        and set color $fish_color_cwd_root
    end
    echo (whoami)@(hostname) [(pwd)] $symbol 
end
