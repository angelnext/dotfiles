return {
  "folke/which-key.nvim",
  event = "VeryLazy",
  init = function()
    vim.o.timeout = true
    vim.o.timeoutlen = 300
  end,
  config = function()
    require("which-key").add({
      { "<leader>b",  group = "buffer" },
      { "<leader>bb", "<cmd>BufferOrderByBufferNumber<cr>",    desc = "Order buffers by number" },
      { "<leader>bd", "<cmd>BufferOrderByDirectory<cr>",       desc = "Order buffers by directory" },
      { "<leader>bl", "<cmd>BufferOrderByLanguage<cr>",        desc = "Order buffers by language" },
      { "<leader>bn", "<cmd>enew<cr>",                         desc = "New buffer" },
      { "<leader>bp", "<cmd>BufferPin<cr>",                    desc = "Pin buffer" },
      { "<leader>bw", "<cmd>BufferOrderByWindowNumber<cr>",    desc = "Order buffers by window number" },
      { "<leader>c",  group = "code" },
      { "<leader>ca", "<cmd>CodeActionMenu<cr>",               desc = "Open code actions menu" },
      { "<leader>cx", require("actions-preview").code_actions, desc = "Toggle trouble nenu" },
      { "<leader>e",  group = "explorer" },
      { "<leader>ef", "<cmd>NvimTreeFocus<cr>",                desc = "Toggle focus" },
      { "<leader>et", "<cmd>NvimTreeToggle<cr>",               desc = "Toggle explorer" },
      { "<leader>f",  group = "file" },
      { "<leader>ff", "<cmd>Telescope find_files<cr>",         desc = "Find files" },
      { "<leader>fh", "<cmd>noh<cr>",                          desc = "Clear search highlight" },
      { "<leader>fr", "<cmd>Telescope oldfiles<cr>",           desc = "Open recent file" },
      { "<leader>fz", "<cmd>ZenMode<cr>",                      desc = "Toggle ZenMode" },
    }, { prefix = "<leader>" })
  end,
}
