return {
  "ray-x/navigator.lua",
  dependencies = {
    { "ray-x/guihua.lua" },
    { "neovim/nvim-lspconfig" },
  },
  config = function()
    local lspconfig = require("lspconfig")

    require("navigator").setup({
      mason = true,
      lsp = {
        denols = {
          root_dir = lspconfig.util.root_pattern("deno.json", "deno.jsonc"),
        },
        tsserver = {
          root_dir = lspconfig.util.root_pattern("package.json"),
          single_file_support = false
        }
      }
    })
  end,
}
